<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * 创建用户操作日志表
 * 可自行扩展
 */
class CreateUserOpLogTable extends Migration
{
    private $tableName;

    public function __construct()
    {
        $this->tableName = env("USER_OP_LOG_TABLE_NAME", 'user_op_logs');
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->charset   = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            $table->engine    = 'InnoDB';
            $table->bigIncrements('id')->nullable(false)->comment('主键,字段不可更改');
            $table->string('op_type', 20)->nullable(false)->comment('操作类型');
            $table->text('op_desc')->nullable(true)->comment('操作描述');
            $table->string('op_result', 80)->nullable(false)->comment('操作结果 success-成功; fail-失败');
            $table->bigInteger('op_user_id')->default(0)->comment('操作用户id');
            $table->string('op_from', 50)->nullable(true)->comment('操作来源');
            $table->text('op_params')->nullable(true)->comment('操作的请求参数');
            $table->text('op_mark')->nullable(true)->comment('操作备注');
            $table->dateTime('create_time')->comment('日志创建时间');
            $table->index(['op_type'], "idx_optype");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
