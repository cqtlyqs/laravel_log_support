<?php

namespace Ktnw\LogSupport\Providers;

use Illuminate\Support\ServiceProvider;

class LogSupportServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes($this->getPublishFiles());
    }

    private function getPublishFiles(): array
    {
        $data       = $this->publishData();
        $srcData    = array_column($data, "src");
        $targetData = array_column($data, "target");
        $publishes  = [];
        for ($i = 0; $i < count($srcData); $i++) {
            $publishes[$srcData[$i]] = $targetData[$i];
        }
        return $publishes;
    }

    private function publishData(): array
    {
        return [
            ['src' => __DIR__ . '/../Console/Commands/LogSupportCommand.php', 'target' => app_path("Console/Commands/LogSupportCommand.php")],
            ['src' => __DIR__ . '/../Middleware/SaveLogMiddleware.php', 'target' => app_path("Http/Middleware/SaveLogMiddleware.php")],
            ['src' => __DIR__ . '/../../database/migrations/2022_02_25_140244_create_user_op_log_table.php', 'target' => database_path("migrations/2022_02_25_140244_create_user_op_log_table.php")],
        ];
    }


}