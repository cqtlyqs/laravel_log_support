<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class LogSupportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'support:log';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update log class namespace.';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'support log';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->namespaceUpdate();
        $this->info($this->type . ' created successfully.');
    }

    private function namespaceUpdate()
    {
        $classes = [
            ["classPath" => 'Http/Middleware/SaveLogMiddleware.php', 'namespace' => 'App\Http\Middleware'],
        ];
        foreach ($classes as $item) {
            $path = app_path($item['classPath']);
            if (!file_exists($path)) {
                $this->info("namespace update fail: file[$path] not exist.");
                return;
            }
            $content = File::get($path);
            File::put($path, preg_replace("/namespace.*/", "namespace " . $item['namespace'] . ";", $content));
        }
    }

}
