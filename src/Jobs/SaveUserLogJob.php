<?php

namespace Ktnw\LogSupport\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class SaveUserLogJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $logParams;

    /**
     * Create a new job instance.
     *
     * @param array $logParams 用户操作日志参数
     */
    public function __construct(array $logParams)
    {
        $this->logParams = $logParams;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        DB::table($this->getUserOpLogTableName())->insertGetId($this->logParams);
    }

    private function getUserOpLogTableName()
    {
        return env("USER_OP_LOG_TABLE_NAME");
    }

}
